dj-database-url==0.4.2
Django==1.11.10
django-filter==1.1.0
djangorestframework==3.7.7
gunicorn==19.7.1
pytz==2017.3
whitenoise==3.3.1
psycopg2==2.7
