# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-02-02 10:39
from __future__ import unicode_literals

from django.db import migrations

def delete_all(apps, schema_editor):
    movie=apps.get_model('catalog', 'Movie')
    movie.objects.all().delete()

class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0009_auto_20180202_1039'),
    ]

    operations = [
        migrations.RunPython(delete_all)
    ]
