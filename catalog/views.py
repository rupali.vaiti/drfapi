# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .serializers import MovieSerializer,GenreSerializer
from rest_framework.views import APIView
from django.shortcuts import render
from .models import *
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from .permissions import *
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.pagination import PageNumberPagination
# Create your views here.
'''class MovieList(APIView):
    permission_classes = (IsAdminOrReadOnly,)
    def get(self, request, format=None):
        movies = Movie.objects.all()
        serializer = MovieSerializer(movies, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = MovieSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class MovieCreate(generics.ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer'''


class MoviePagination(PageNumberPagination):
    page_size = 10

class MovieFilterView(generics.ListAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('name', 'director','imdb_score','popularity')
    pagination_class = MoviePagination

class MovieListView(generics.ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer



class MovieView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()


class GenresListView(generics.ListCreateAPIView):
    queryset = Genre.objects.all()
    serializer_class = GenreSerializer


class GenresView(generics.RetrieveDestroyAPIView):
    serializer_class = GenreSerializer
    queryset = Genre.objects.all()

class GenresShowView(generics.RetrieveAPIView):
    serializer_class = GenreSerializer
    queryset = Genre.objects.all()


class index(generics.RetrieveAPIView):
    """
    A view that returns a templated HTML representation of a given user.    """

    renderer_classes = (TemplateHTMLRenderer,)

    def get(self, request, *args, **kwargs):
        queryset = Movie.objects.all()
        queryset_gen = Genre.objects.all()

        return Response({'movielist':queryset,'gen':queryset_gen},template_name='index.html')
