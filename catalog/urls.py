from django.conf.urls import url
from .views import *
from django.contrib.auth import views
app_name='catalog'
urlpatterns=[

    url(r'^index/$',index.as_view(),name='index'),

    url(r'^movieslist/$', MovieFilterView.as_view(), name='movies'),
    url(r'^movies/$',MovieListView.as_view(),name='movielist'),
    url(r'^movies/(?P<pk>\d+)/$', MovieView.as_view(),name='movieedit'),
    url(r'^genres/$', GenresListView.as_view(),name='genreslist'),
    url(r'^genresshow/(?P<pk>\d+)/$', GenresShowView.as_view(),name='genresshow'),
    url(r'^genres/(?P<pk>\d+)/$', GenresView.as_view(),name='genresedit'),





]