from .models import Movie,Genre
from rest_framework import serializers


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('id', 'category','movie')

class MovieSerializer(serializers.ModelSerializer):
    genres = serializers.StringRelatedField(many=True)
    # genres = serializers.StringRelatedField(many=True)


    class Meta:
        model = Movie
        fields = ('id','name', 'director', 'imdb_score','popularity','genres')

    def create(self, validated_data):
        gen_data = validated_data.pop('genres')
        movie_obj = Movie.objects.create(**validated_data)
        for each_data in gen_data:
            Genre.objects.create(movie=movie_obj, **each_data)
        return movie_obj

    def update(self, instance, validated_data):
        gen_data = validated_data.pop('genres')
        genress = (instance.genres).all()
        genress = list(genress)
        instance.name = validated_data.get('name', instance.name)
        instance.director = validated_data.get('director', instance.director)
        instance.imdb_score = validated_data.get('imdb_score', instance.imdb_score)
        instance.popularity = validated_data.get('popularity', instance.popularity)
        instance.save()

        for each_data in gen_data:
            gen = genress.pop(0)
            gen.category = each_data.get('category', gen.category)
            gen.save()
        return instance


