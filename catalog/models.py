# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

types=[(u'biography', u'Biography'), (u'horror', u'Horror'), (u'crime', u'Crime'), (u'romance', u'Romance'), (u'sport', u'Sport'), (u'animation', u'Animation'), (u'adult', u'Adult'), (u'adventure', u'Adventure'), (u'news', u'News'), (u'western', u'Western'), (u'comedy', u'Comedy'), (u'reality-tv', u'Reality-TV'), (u'thriller', u'Thriller'), (u'short', u'Short'), (u'sci-fi', u'Sci-Fi'), (u'horror', u'Horror'), (u'film-noir', u'Film-Noir'), (u'mystery', u'Mystery'), (u'history', u'History'), (u'musical', u'Musical'), (u'drama', u'Drama'), (u'adventure', u'Adventure'), (u'action', u'Action'), (u'documentary', u'Documentary'), (u'thriller', u'Thriller'), (u'western', u'Western'), (u'mystery', u'Mystery'), (u'short', u'Short'), (u'family', u'Family'), (u'talk-show', u'Talk-Show'), (u'film-noir', u'Film-Noir'), (u'drama', u'Drama'), (u'action', u'Action'), (u'documentary', u'Documentary'), (u'musical', u'Musical'), (u'fantasy', u'Fantasy'), (u'family', u'Family'), (u'talk-show', u'Talk-Show'), (u'crime', u'Crime'), (u'fantasy', u'Fantasy'), (u'game-show', u'Game-Show'), (u'music', u'Music'), (u'comedy', u'Comedy'), (u'war', u'War'), (u'biography', u'Biography'), (u'romance', u'Romance')]


class Movie(models.Model):
    name = models.CharField(max_length=255)
    director = models.CharField(max_length=255)
    imdb_score = models.DecimalField(decimal_places=2, max_digits=4)
    popularity= models.DecimalField(decimal_places=2, max_digits=4)

    def __unicode__(self):
        return '{}'.format(self.name)

class Genre(models.Model):
    movie = models.ForeignKey(Movie, related_name='genres',on_delete=models.CASCADE)
    category = models.CharField(max_length=255,choices=types)

    def __unicode__(self):
        return '{}'.format(self.category)
